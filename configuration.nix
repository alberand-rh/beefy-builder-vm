{ config, pkgs, lib, modulesPath, inputs, ... }:
{

  imports = [
    ./hardware-configuration.nix
  ];

  networking.hostName = "nix-builder";
  networking.networkmanager.enable = true;

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.kernelParams = ["console=ttyS0,115200n8"];

  environment.systemPackages = with pkgs; [
    neovim
    vim
    hello
    git
  ];

  services.sshd.enable = true;
  services.openssh.enable = true;

  users.users.nixremote = {
    isNormalUser = true;
    description = "Distributed Builder";
    extraGroups = [ "wheel" "networkmanager" ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDaAJ7OO1fezINthnc3A1vUPr8h1tilt4RkCiwldhRU6FjxcxtPlgye/Wm+xxVgjOTelZZ0jq0hSSPk2cqTdNJ6g50OWTWvpbHEJ5M71TME0vnfxap+CuSv+PAHerNuoGEqHqFUEJTzn6r/7vcKRri93BekKl3n+BgqVPB1jCAymDKXAVYAwxPq1Up691zPBOmpjMalE7u5Zfxt+dkN17MA946dLVlzqWTZKbkpI1m+QZKEJaHXE792KU4uPW6vhZM5L7SveI2hwN1iHq+XlmjeyBFSsGLR+sR2HRy/Q+F6ufTW9nFMT/TSEXbZj1R6IFNxcnmnhOG61ZnKQqs04fk+njnQGqEjMGt4lJcCTdXp9tGlQQNYYdZKRgiqAnfh63vtVluFpvb0U0udWZ3d3aRHhcsNsk9MQ6EMgYLeW+5Xf23p8TKVYgpdprcU9t6f/3pKt6xLPH3LJMN6myLUvTt6cyZKext1C7LhfsWv1BnDe5DOijE8OT4xbZkHKIuphs0= nixremote@nixxy"
    ];
  };

  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    trusted-users = [ "root" "nixremote" ];
  };

  system.stateVersion = "23.05";
}
