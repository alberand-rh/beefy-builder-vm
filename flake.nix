{
  description = "Kernel Builder Machine";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-generators.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nixos-generators }:
  let
    pkgs = import nixpkgs {
      inherit system;
    };
    lib = nixpkgs.lib;
    system = "x86_64-linux";
    modules = [
      ./configuration.nix
    ];
  in rec {
    nixosConfigurations."builder" = lib.nixosSystem {
        inherit pkgs;
        inherit system;
        inherit modules;
    };
    builder-iso = nixos-generators.nixosGenerate {
      inherit modules;
      system = "x86_64-linux";
      format = "iso";
    };
    packages.default = builder-iso;
  };
}

